package evans.javacollections;
/**
 * Evans_Jake_Week2.java
 * Course: CIT360
 * Name: Jake Evans
 */
import java.util .*;
public class JavaCollections {
    public static void main(String[] args) {
        // I got the idea for this from watching your video
        System.out.println("---->Java List<----");
        List list = new ArrayList();
        list.add("This");
        list.add("is");
        list.add("my");
        list.add("Java");
        list.add("list.");
        for (Object str : list) {
            System.out.println((String) str);
        }

        System.out.println("---->Java Set<----");
        Set set = new TreeSet();
        set.add("This");
        set.add("is");
        set.add("my");
        set.add("Java");
        set.add("set.");
        for (Object str : set) {
            System.out.println((String) str);
        }
        System.out.println("---->Java Map<----");
        Map map = new HashMap();
        // The Map is cool because you can specify the Map order.
        map.put(2, "is");
        map.put(1, "This");
        map.put(4, "Java");
        map.put(5, "map.");
        map.put(3, "my");
        for (int i = 1; i < 6; i++) {
            String result = (String) map.get(i);
            System.out.println(result);
        }
        System.out.println("---->Java Queue<----");
        Queue queue = new PriorityQueue();
        // I numbered the lines so it would show up in the correct order
        queue.add("5 queue.");
        queue.add("4 Java");
        queue.add("3 my");
        queue.add("1 This");
        queue.add("2 is");
        // I used the Iterator so I could use queue.poll and print it in order
        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }
        System.out.println("---->Generic Java List<----");
        List<Books> myBooks = new LinkedList<Books>();
        myBooks.add(new Books("Wild Fire", "Nelson DeMille"));
        myBooks.add(new Books("Murder at the Opera", "Margaret Truman"));
        myBooks.add(new Books("Santa Cruise", "Carol Higgins Clark"));
        myBooks.add(new Books("The Woods", "Harlan Coben"));
        for (Books books : myBooks) {
            System.out.println(books);
        }
    }
}




